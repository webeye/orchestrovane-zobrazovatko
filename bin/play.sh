#!/bin/sh

STREAM=$1

/usr/bin/killall -9 /usr/bin/omxplayer.bin 2>/dev/null >/dev/null; 

[ -z "$STREAM" ] && exit 1
sleep 1; 

LD_LIBRARY_PATH=/opt/vc/lib:/usr/lib/omxplayer /usr/bin/omxplayer.bin -o hdmi ${STREAM} -b

