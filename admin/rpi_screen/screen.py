__author__ = 'rak'

import requests
from django.conf import settings
import json


class RPiScreen(object):

    _screen_name = ""

    _group_name = ""

    _ip = ""

    def __init__(self, screen_name, ip):
        self._screen_name = screen_name
        self.ip_addr(ip)

    def group(self, group_name):
        self._group_name = group_name
        return self

    def ip_addr(self, ip_addr):
        self._ip = ip_addr
        return self

    def register(self):
        api_url = "%s/" % "/".join([settings.SERVER_API_URL, 'screen', 'register'])
        data = {
            'name': self._screen_name,
            'group': self._group_name,
            'ip' : self._ip
        }

        response = requests.post(api_url, json.dumps(data))
        print response.content
