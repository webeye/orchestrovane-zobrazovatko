__author__ = 'rak'

from django.core.management.base import BaseCommand
import requests
from admin.rpi_screen.screen import RPiScreen
from optparse import make_option


class Command(BaseCommand):
    args = '[screen name]'
    help = 'register and create screen into control'
    image_archive = None
    option_list = BaseCommand.option_list + (
        make_option('--ip', action='store', dest='ip', default=False, help='RPi IP address'),
    )

    def __init__(self):
        super(Command, self).__init__()

    def handle(self, *args, **options):
        if len(args) < 1:
            raise BaseException("Invalid number of arguments")

        rpi = RPiScreen(args[0], options['ip'])
        if len(args) < 2:
            rpi.group(args[1])

        rpi.register()
