/*
 ===================================================================================================
 aGRID - JavaScript IE (2015/05/05)
 ===================================================================================================
 */

(function() {
	var arrayPrototype = Array.prototype,
		stringPrototype = String.prototype,
		functionPrototype = Function.prototype,
		datePrototype = Date.prototype,
		objectPrototype = Object.prototype,
		slice = arrayPrototype.slice,
		hasOwnProperty = objectPrototype.hasOwnProperty,
		toString = objectPrototype.toString;

	if (!Object.create) {
		Object.create = function(proto) {
			var f = function() {
			};
			if (typeof proto !== 'object') {
				throw TypeError();
			}
			f.prototype = proto;
			return new f();
		};
	}

	if (!Object.defineProperty) {
		Object.defineProperty = function(obj, prop, descriptor) {
			if (typeof obj !== 'object') {
				throw TypeError();
			}
			if (objectPrototype.__defineGetter__ && ('get' in descriptor)) {
				objectPrototype.__defineGetter__.call(obj, prop, descriptor.get);
			}
			if (objectPrototype.__defineSetter__ && ('set' in descriptor)) {
				objectPrototype.__defineSetter__.call(obj, prop, descriptor.set);
			}
			if ('value' in descriptor) {
				obj[prop] = descriptor.value;
			}
			return obj;
		};
	}

	if (!Object.defineProperties) {
		Object.defineProperties = function(obj, props) {
			if (typeof obj !== 'object') {
				throw TypeError();
			}
			for (var p in props) {
				if (hasOwnProperty.call(props, p)) {
					Object.defineProperty(obj, p, props[p]);
				}
			}
			return obj;
		};
	}

	if (!Object.getPrototypeOf) {
		Object.getPrototypeOf = function(obj) {
			if (typeof obj !== 'object') {
				throw TypeError();
			}
			return obj.__proto__ || obj.constructor.prototype || objectPrototype;
		};
	}

	if (!Object.keys) {
		Object.keys = function(obj) {
			if (typeof obj !== 'object') {
				throw TypeError();
			}
			var h = !({
				toString: null
			}).propertyIsEnumerable('toString'),
				d = [
					'toString',
					'toLocaleString',
					'valueOf',
					'hasOwnProperty',
					'isPrototypeOf',
					'propertyIsEnumerable',
					'constructor'
				],
				l = d.length,
				a = [];
			for (var p in obj) {
				if (hasOwnProperty.call(obj, p)) {
					a.push(p);
				}
			}
			if (h) {
				for (var i = 0; i < l; i++) {
					if (hasOwnProperty.call(obj, d[i])) {
						a.push(d[i]);
					}
				}
			}
			return a;
		};
	}

	if (!Object.getOwnPropertyDescriptor) {
		Object.getOwnPropertyDescriptor = function(obj, prop) {
			if (typeof obj !== 'object') {
				throw TypeError();
			}
			if (hasOwnProperty.call(obj, prop)) {
				return {
					value: obj[prop],
					enumerable: true,
					writable: true,
					configurable: true
				};
			}
		};
	}

	if (!Object.getOwnPropertyNames) {
		Object.getOwnPropertyNames = function(obj) {
			if (typeof obj !== 'object') {
				throw TypeError();
			}
			var a = [];
			for (var p in obj) {
				if (hasOwnProperty.call(obj, p)) {
					a.push(p);
				}
			}
			return a;
		};
	}

	if (!arrayPrototype.indexOf) {
		arrayPrototype.indexOf = function(searchElement, fromIndex) {
			if (this === null) {
				throw new TypeError();
			}
			var o = Object(this),
				l = o.length >>> 0,
				n;
			if (l === 0) {
				return -1;
			}
			n = +fromIndex || 0;
			if (Math.abs(n) === Infinity) {
				n = 0;
			}
			if (n >= l) {
				return -1;
			}
			for (var i = Math.max(n >= 0 ? n : l - Math.abs(n), 0); i < l; i++) {
				if (i in o && o[i] === searchElement) {
					return i;
				}
			}
			return -1;
		};
	}

	if (!arrayPrototype.lastIndexOf) {
		arrayPrototype.lastIndexOf = function(searchElement) {
			if (this === null) {
				throw new TypeError();
			}
			var o = Object(this),
				l = o.length >>> 0,
				n;
			if (l === 0) {
				return -1;
			}
			n = l - 1;
			if (arguments.length > 1) {
				n = Number(arguments[1]);
				if (n !== n) {
					n = 0;
				} else if (n !== 0 && n !== (1 / 0) && n !== -(1 / 0)) {
					n = (n > 0 || -1) * Math.floor(Math.abs(n));
				}
			}
			for (var i = n >= 0 ? Math.min(n, l - 1) : l - Math.abs(n); i >= 0; i--) {
				if (i in o && o[i] === searchElement) {
					return i;
				}
			}
			return -1;
		};
	}

	if (!arrayPrototype.every) {
		arrayPrototype.every = function(callback, thisArg) {
			if (this === null || typeof callback !== 'function') {
				throw new TypeError();
			}
			var o = Object(this),
				l = o.length >>> 0,
				n;
			if (arguments.length > 1) {
				n = thisArg;
			}
			for (var i = 0; i < l; i++) {
				if (i in o) {
					var r = callback.call(n, o[i], i, o);
					if (!r) {
						return false;
					}
				}
			}
			return true;
		};
	}

	if (!arrayPrototype.some) {
		arrayPrototype.some = function(callback, thisArg) {
			if (this === null || typeof callback !== 'function') {
				throw new TypeError();
			}
			var o = Object(this),
				l = o.length >>> 0,
				n;
			if (arguments.length > 1) {
				n = thisArg;
			}
			for (var i = 0; i < l; i++) {
				if (i in o && callback.call(n, o[i], i, o)) {
					return true;
				}
			}
			return false;
		};
	}

	if (!arrayPrototype.forEach) {
		arrayPrototype.forEach = function(callback, thisArg) {
			if (this === null || typeof callback !== 'function') {
				throw new TypeError();
			}
			var o = Object(this),
				l = o.length >>> 0,
				n;
			if (arguments.length > 1) {
				n = thisArg;
			}
			for (var i = 0; i < l; i++) {
				if (i in o) {
					callback.call(n, o[i], i, o);
				}
			}
			return true;
		};
	}

	if (!arrayPrototype.map) {
		arrayPrototype.map = function(callback, thisArg) {
			if (this === null || typeof callback !== 'function') {
				throw new TypeError();
			}
			var o = Object(this),
				l = o.length >>> 0,
				n,
				a;
			if (arguments.length > 1) {
				n = thisArg;
			}
			a = new Array(l);
			for (var i = 0; i < l; i++) {
				if (i in o) {
					a[i] = callback.call(n, o[i], i, o);
				}
			}
			return a;
		};
	}

	if (!arrayPrototype.filter) {
		arrayPrototype.filter = function(callback, thisArg) {
			if (this === null || typeof callback !== 'function') {
				throw new TypeError();
			}
			var o = Object(this),
				l = o.length >>> 0,
				n,
				a;
			if (arguments.length > 1) {
				n = thisArg;
			}
			a = [];
			for (var i = 0; i < l; i++) {
				if (i in o && callback.call(thisArg, o[i], i, o)) {
					a.push(o[i]);
				}
			}
			return a;
		};
	}

	if (!arrayPrototype.reduce) {
		arrayPrototype.reduce = function(callback, initialValue) {
			if (this === null || typeof callback !== 'function') {
				throw new TypeError();
			}
			var o = Object(this),
				l = o.length >>> 0,
				n;
			if (arguments.length > 1) {
				n = initialValue;
			}
			for (var i = 0; i < l; i++) {
				if (i in o && n !== undefined) {
					n = callback(n, o[i], i, o);
				} else {
					n = o[i];
				}
			}
			return n;
		};
	}

	if (!arrayPrototype.reduceRight) {
		arrayPrototype.reduceRight = function(callback, initialValue) {
			if (this === null || typeof callback !== 'function') {
				throw new TypeError();
			}
			var o = Object(this),
				l = o.length >>> 0,
				n;
			if (arguments.length > 1) {
				n = initialValue;
			}
			for (var i = l - 1; -1 < i; --i) {
				if (i in o && n !== undefined) {
					n = callback(n, o[i], i, o);
				} else {
					n = o[i];
				}
			}
			return n;
		};
	}

	if (!Array.isArray) {
		Array.isArray = function(obj) {
			return toString.call(obj) === '[object Array]';
		};
	}

	if (!stringPrototype.trim) {
		stringPrototype.trim = function() {
			return this.replace(/^\s+|\s+$/g, '');
		};
	}

	if (!functionPrototype.bind) {
		functionPrototype.bind = function(thisArg) {
			if (typeof this !== 'function') {
				throw new TypeError();
			}
			var a = slice.call(arguments, 1),
				t = this,
				n = function() {
				},
				b = function() {
					return t.apply(this instanceof n && thisArg ? this : thisArg, a.concat(slice.call(arguments)));
				};
			n.prototype = this.prototype;
			b.prototype = new n();
			return b;
		};
	}

	if (!datePrototype.toISOString) {
		datePrototype.toISOString = function() {
			var f = function(n) {
				if (n < 10) {
					return '0' + n;
				}
				return n;
			};
			return this.getUTCFullYear() + '-' + f(this.getUTCMonth() + 1) + '-' + f(this.getUTCDate()) + 'T' + f(this.getUTCHours()) + ':' + f(this.getUTCMinutes()) + ':' + f(this.getUTCSeconds()) + '.' + (this.getUTCMilliseconds() / 1000).toFixed(3).slice(2, 5) + 'Z';
		};
	}

	if (!datePrototype.toJSON) {
		datePrototype.toJSON = datePrototype.toISOString;
	}

	if (!Date.now) {
		Date.now = function() {
			return new Date().getTime();
		};
	}

	if (!window.JSON) {
		window.JSON = {
			parse: function(text) {
				return eval('(' + text + ')');
			},
			stringify: function(value) {
				if (value instanceof Object) {
					var o = '';
					if (value.constructor === Array) {
						for (var i = 0; i < value.length; i++) {
							o += this.stringify(value[i]) + ',';
						}
						return '[' + o.substr(0, o.length - 1) + ']';
					}
					if (value.toString !== toString) {
						return '\"' + value.toString().replace(/"/g, '\\$&') + '\"';
					}
					for (var p in value) {
						if (hasOwnProperty.call(value, p)) {
							o += '\"' + p.replace(/"/g, '\\$&') + '\":' + this.stringify(value[p]) + ',';
						}
					}
					return '{' + o.substr(0, o.length - 1) + '}';
				}
				return typeof value === 'string' ? '\"' + value.replace(/"/g, '\\$&') + '\"' : String(value);
			}
		};
	}

})();
