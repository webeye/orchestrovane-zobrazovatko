(function($) {
	'use strict';
	window.aoz = {
		options: {
			url: location.protocol + '//' + location.host + '/api/v1/',
			contentType: 'application/json; charset=utf-8',
			itemContent: '.item-content',
			itemContentEdit: '.js-item-content-edit',
			itemContentRemove: '.js-item-content-remove',
			col: '.js-col',
			interval: 60000,
			callback: 0
		},
		dom: {
			$refresh: $('#js-refresh'),
			$resource: $('#js-resource'),
			$resourceStart: $('#js-resource-start'),
			$resourceAdd: $('#js-resource-add'),
			$resourceForm: $('#js-resource-form'),
			$resourceId: $('#js-resource-id'),
			$resourceType: $('.js-resource-type'),
			$resourceDefinition: $('#js-resource-definition'),
			$playlist: $('#js-playlist'),
			$playlistStart: $('#js-playlist-start'),
			$playlistAdd: $('#js-playlist-add'),
			$playlistForm: $('#js-playlist-form'),
			$playlistId: $('#js-playlist-id'),
			//@todo
			$screenGroup: $('#js-screen-group'),
			$screenGroupStart: $('#js-screen-group-start'),
			$screenGroupAdd: $('#js-screen-group-add'),
			$screenGroupForm: $('#js-screen-group-form'),
			$screenGroupId: $('#js-screen-group-id'),
			//@todo
			$overlay: $('#js-overlay'),
			$dialogClose: $('#js-dialog-close'),
			$dialogContent: $('.js-dialog-content')
		},
		/**
		 * Initialize
		 */
		init: function() {
			var root = this;

			root.dom.$resourceAdd.on('click', function(event) {
				event.preventDefault();
				root.dom.$resourceForm.show().data('type', 'post');
				root.dom.$overlay.show();
			});

			root.dom.$resource.on('click', root.options.itemContentEdit, function(event) {
				event.preventDefault();
				root.getResource($(this).data('location'));
			});

			root.dom.$resourceForm.on('submit', function(event) {
				var type = root.dom.$resourceForm.data('type'),
					location = $(this).data('location'),
					resource = {
						user_stream_id: root.dom.$resourceId.val(),
						type: root.dom.$resourceType.filter(':checked').val(),
						definition: root.dom.$resourceDefinition.val()
					};
				event.preventDefault();
				if (type === 'post') {
					root.postResource.call(root, resource);
				} else if (type === 'put') {
					root.putResource.call(root, resource, location);
				}
			});

			root.dom.$resource.on('click', root.options.itemContentRemove, function(event) {
				event.preventDefault();
				root.deleteResource.call(root, $(this).data('location'));
			});

			root.dom.$playlistAdd.on('click', function(event) {
				event.preventDefault();
				root.dom.$playlistForm.show().data('type', 'post');
				root.dom.$overlay.show();
			});

			root.dom.$playlist.on('click', root.options.itemContentEdit, function(event) {
				event.preventDefault();
				root.getPlaylist($(this).data('location'));
			});

			root.dom.$playlistForm.on('submit', function(event) {
				var type = root.dom.$playlistForm.data('type'),
					location = $(this).data('location'),
					playlist = {
						playlist_name: root.dom.$playlistId.val()
					};
				event.preventDefault();
				if (type === 'post') {
					root.postPlaylist.call(root, playlist);
				} else if (type === 'put') {
					root.putPlaylist.call(root, playlist, location);
				}
			});

			root.dom.$playlist.on('click', root.options.itemContentRemove, function(event) {
				event.preventDefault();
				root.deletePlaylist.call(root, $(this).data('location'));
			});

			root.dom.$screenGroupAdd.on('click', function(event) {
				event.preventDefault();
				root.dom.$screenGroupForm.show().data('type', 'post');
				root.dom.$overlay.show();
			});

			root.dom.$screenGroup.on('click', root.options.itemContentEdit, function(event) {
				event.preventDefault();
				root.getScreenGroup($(this).data('location'));
			});

			root.dom.$screenGroupForm.on('submit', function(event) {
				var type = root.dom.$screenGroupForm.data('type'),
					location = $(this).data('location'),
					screenGroup = {
						group_name: root.dom.$screenGroupId.val()
					};
				event.preventDefault();
				if (type === 'post') {
					root.postScreenGroup.call(root, screenGroup);
				} else if (type === 'put') {
					//@todo
					root.putScreenGroup.call(root, screenGroup, location);
				}
			});

			root.dom.$screenGroup.on('click', root.options.itemContentRemove, function(event) {
				event.preventDefault();
				root.deleteScreenGroup.call(root, $(this).data('location'));
			});

			root.dom.$dialogClose.on('click close', function(event) {
				event.preventDefault();
				root.dom.$dialogContent.hide().removeData().find('input[type="text"], textarea').val('');
				root.dom.$overlay.hide();
			});

			root.dom.$refresh.on('click refresh', function(event) {
				event.preventDefault();
				clearTimeout(root.options.callback);
				root.getResources.call(root);
				root.getPlaylists.call(root);
				root.getScreenGroups.call(root);
				root.options.callback = setTimeout(function() {
					root.dom.$refresh.trigger('refresh');
				}, root.options.interval);

			});

			root.dom.$refresh.trigger('refresh');
		},
		/**
		 * New item factory
		 * @param {string} location
		 * @param {string} name
		 */
		itemFactory: function(location, name) {
			if (typeof location === 'string' && typeof name === 'string') {
				return $('<div class="col-6 m-col-12 s-col-12 js-col"><div class="item"><div class="item-content"><a class="js-item-content-remove item-content-remove" href="#" title="Remove" data-location="' + location + '"></a><a class="js-item-content-edit item-content-edit" href="#" title="Edit" data-location="' + location + '"></a><span class="item-content-name">' + name + '</span></div></div></div>');
			} else {
				throw new Error('Invalid method arguments');
			}
		}
	};
})(jQuery);
