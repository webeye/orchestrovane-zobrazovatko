(function($) {
	'use strict';

	/**
	 * Add playlist item
	 * @param {Object} playlistItem
	 */
	window.aoz.postPlaylistItem = function(playlistItem) {
		if (typeof playlistItem === 'object') {
			var root = this;
			$.ajax({
				type: 'POST',
				url: root.options.url + 'playlistitem/',
				data: JSON.stringify(playlistItem),
				contentType: root.options.contentType,
				success: function(data, textStatus, jqXHR) {
					root.dom.$refresh.trigger('refresh');
					root.dom.$dialogClose.trigger('close');
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		} else {
			throw new Error('Invalid method arguments');
		}
	};

})(jQuery);