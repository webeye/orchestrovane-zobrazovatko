(function($) {
	'use strict';

	/**
	 * Get screen group
	 */
	window.aoz.getScreenGroups = function() {
		var root = this;
		clearInterval(root.options.callback);
		root.dom.$screenGroup.find(root.options.col).remove();
		$.ajax({
			type: 'GET',
			url: root.options.url + 'screengroup/',
			contentType: root.options.contentType,
			success: function(data) {
				var dataObjectsLength = data.objects.length,
					dataObject = {};
				for (var i = 0; i < dataObjectsLength; i++) {
					dataObject = data.objects[i];
					root.dom.$screenGroupStart.after(root.itemFactory.call(root, dataObject.resource_uri, dataObject.group_name + '<br /> (screen group)'));
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(errorThrown);
			}
		});

	};

	/**
	 * Get screen group
	 * @param {string} location
	 */
	window.aoz.getScreenGroup = function(location) {
		if (typeof location === 'string') {
			var root = this;
			$.ajax({
				type: 'GET',
				url: location,
				contentType: root.options.contentType,
				success: function(data) {
					root.dom.$screenGroupId.val(data.group_name);
					root.dom.$screenGroupForm.show().data('type', 'put').data('location', location);
					root.dom.$overlay.show();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		} else {
			throw new Error('Invalid method arguments');
		}
	};

	/**
	 * Add screen group
	 * @param {Object} screenGroup
	 */
	window.aoz.postScreenGroup = function(screenGroup) {
		if (typeof screenGroup === 'object') {
			var root = this;
			$.ajax({
				type: 'POST',
				url: root.options.url + 'screengroup/',
				data: JSON.stringify(screenGroup),
				contentType: root.options.contentType,
				success: function() {
					root.dom.$refresh.trigger('refresh');
					root.dom.$dialogClose.trigger('close');
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		} else {
			throw new Error('Invalid method arguments');
		}
	};

	/**
	 * Edit screen group
	 * @param {Object} screenGroup
	 * @param {string} location
	 */
	window.aoz.putScreenGroup = function(screenGroup, location) {
		if (typeof screenGroup === 'object' && typeof location === 'string') {
			var root = this;
			$.ajax({
				type: 'PUT',
				url: location,
				data: JSON.stringify(screenGroup),
				contentType: root.options.contentType,
				success: function() {
					root.dom.$refresh.trigger('refresh');
					root.dom.$dialogClose.trigger('close');
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		} else {
			throw new Error('Invalid method arguments');
		}
	};

	/**
	 * Remove screen group
	 * @param {string} location
	 */
	window.aoz.deleteScreenGroup = function(location) {
		if (typeof location === 'string') {
			var root = this;
			$.ajax({
				type: 'DELETE',
				url: location,
				contentType: root.options.contentType,
				success: function() {
					root.dom.$refresh.trigger('refresh');
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		} else {
			throw new Error('Invalid method arguments');
		}
	};

})(jQuery);
