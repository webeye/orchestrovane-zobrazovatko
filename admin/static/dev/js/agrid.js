/*
 ===================================================================================================
 aGRID - JavaScript (2015/04/17)
 ===================================================================================================
 */

(function() {
	document.body.className = document.body.className.replace('no-js', 'js no-load');

	window.onload = function() {
		document.body.className = document.body.className.replace('no-load', 'load');
	};

	window.aGRID = {
		options: {
			console: false,
			checkInterval: 100,
			entityMap: {
				'&': '&amp;',
				'<': '&lt;',
				'>': '&gt;',
				'"': '&quot;',
				'\'': '&#39;',
				'/': '&#x2F;'
			},
			entityEscapeRegExp: /[&<>"'\/]/g,
			entityUnescapeRegExp: /(&amp;)|(&lt;)|(&gt;)|(&quot;)|(&#39;)|(&#x2F;)/g,
			isDesktop: false,
			isDesktopExecuted: false,
			isMobile: false,
			isMobileExecuted: false,
			log: []
		}
	};

	/**
	 * Add event to element
	 * @param {object} element
	 * @param {string} type
	 * @param {function} handler
	 */
	aGRID.addEvent = function(element, type, handler) {
		if (element.addEventListener) {
			element.addEventListener(type, handler, false);
		} else if (element.attachEvent) {
			element.attachEvent('on' + type, handler);
		}
	};

	/**
	 * Add device specific functions
	 * @param {object} handlers
	 * @param {number} [interval=100]
	 */
	aGRID.responsive = function(handlers, interval) {
		var root = this,
			deviceCheck;
		interval = interval || 100;
		if (typeof handlers === 'object' && typeof interval === 'number') {
			deviceFunction();

			root.addEvent(window, 'resize', function() {
				clearTimeout(deviceCheck);
				deviceCheck = setTimeout(deviceFunction, interval);
			});

		}

		function deviceFunction() {
			var isMobileOld = root.options.isMobile;
			var isDesktopOld = root.options.isDesktop;
			root.options.isMobile = typeof window.getComputedStyle === 'function' ? window.getComputedStyle(document.body, ':after').getPropertyValue('content').replace(/"/g, '') === 'small' : false;
			root.options.isDesktop = !root.options.isMobile;
			if (root.options.isDesktop && !root.options.isDesktopExecuted) {
				if (typeof handlers.desktopOnce === 'function') {
					handlers.desktopOnce();
				}
				root.options.isDesktopExecuted = true;
			} else if (root.options.isMobile && !root.options.isMobileExecuted) {
				if (typeof handlers.mobileOnce === 'function') {
					handlers.mobileOnce();
				}
				root.options.isMobileExecuted = true;
			}
			if (root.options.isDesktop && !isDesktopOld && typeof handlers.desktopOnly === 'function') {
				handlers.desktopOnly();
			} else if (root.options.isMobile && !isMobileOld && typeof handlers.mobileOnly === 'function') {
				handlers.mobileOnly();
			}
		}

	};

	/**
	 * Initialize Google Analytics
	 * @param {string} account
	 */
	aGRID.googleAnalytics = function(account) {
		if (typeof account === 'string') {
			var newScript = document.createElement('script'),
				firstScript = document.getElementsByTagName('script')[0];
			window._gaq = window._gaq || [];
			window._gaq.push(['_setAccount', account]);
			window._gaq.push(['_trackPageview']);
			newScript.async = true;
			newScript.src = '//www.google-analytics.com/ga.js';
			firstScript.parentNode.insertBefore(newScript, firstScript);
		}
	};

	/**
	 * Initialize Universal Analytics
	 * @param {string} account
	 */
	aGRID.universalAnalytics = function(account) {
		if (typeof account === 'string') {
			var newScript = document.createElement('script'),
				firstScript = document.getElementsByTagName('script')[0];
			window.GoogleAnalyticsObject = 'ga';
			window.ga = window.ga || function() {
				window.ga.q = window.ga.q || [];
				window.ga.q.push(arguments);
			};
			window.ga.l = +new Date();
			newScript.async = true;
			newScript.src = '//www.google-analytics.com/analytics.js';
			firstScript.parentNode.insertBefore(newScript, firstScript);
			window.ga('create', account, 'auto');
			window.ga('send', 'pageview');
		}
	};

	/**
	 * Initialize Google Maps
	 * @param {string} account
	 * @param {string} [version='3']
	 * @param {string} [callback='googleMap']
	 */
	aGRID.googleMap = function(account, version, callback) {
		version = version || '3';
		callback = callback || 'googleMap';
		if (typeof account === 'string' && typeof version === 'string' && typeof callback === 'string') {
			var newScript = document.createElement('script'),
				firstScript = document.getElementsByTagName('script')[0];
			newScript.src = 'https://maps.googleapis.com/maps/api/js?v=' + version + '&key=' + account + '&callback=' + callback;
			firstScript.parentNode.insertBefore(newScript, firstScript);
		}
	};

	/**
	 * Get random number from interval [min, max)
	 * @param {number} min
	 * @param {number} max
	 * @returns {number}
	 */
	aGRID.randomNumber = function(min, max) {
		if (typeof min === 'number' && typeof max === 'number') {
			return Math.random() * (max - min) + min;
		} else {
			return Math.random();
		}
	};

	/**
	 * Get random integer from interval [min, max)
	 * @param {number} min
	 * @param {number} max
	 * @returns {number}
	 */
	aGRID.randomInteger = function(min, max) {
		if (typeof min === 'number' && typeof max === 'number') {
			return Math.floor(Math.random() * (max - min) + min);
		} else {
			return Math.floor(Math.random() * 2);
		}
	};

	/**
	 * Get key by value
	 * @param {string} value
	 * @param {object} object
	 * @returns {string}
	 */
	aGRID.getKeyByValue = function(value, object) {
		if (typeof value !== 'undefined' && typeof object !== 'undefined') {
			for (var key in object) {
				if (object.hasOwnProperty(key)) {
					if (object[key] === value) {
						return key;
					}
				}
			}
		}
	};

	/**
	 * Get value by key
	 * @param {string} key
	 * @param {object} object
	 * @returns {*}
	 */
	aGRID.getValueByKey = function(key, object) {
		if (typeof key !== 'undefined' && typeof object !== 'undefined') {
			return object[key];
		}
	};

	/**
	 * Serialize URL parameters from Object to String
	 * @param {object} params
	 * @returns {string}
	 */
	aGRID.serializeUrl = function(params) {
		var url = '';
		if (typeof params === 'object') {
			for (var param in params) {
				if (params.hasOwnProperty(param)) {
					if (url !== '') {
						url += '&';
					}
					if (typeof params[param] !== 'undefined' && params[param] !== null) {
						url += encodeURIComponent(param) + '=' + encodeURIComponent(params[param]);
					}
				}
			}
		}
		return url;
	};

	/**
	 * Unserialize URL parameters from String to Object
	 * @param {string} [url=window.location.href]
	 * @returns {object}
	 */
	aGRID.unserializeUrl = function(url) {
		var params = {};
		url = typeof url !== 'undefined' ? url : window.location.href;
		if (typeof url === 'string' && url.indexOf('?') !== -1) {
			url = url.replace(/(\#(.*))|((.*)\?)/g, '').split('&');
			for (var param in url) {
				if (url.hasOwnProperty(param)) {
					param = url[param].split('=');
					if (param[0] !== '') {
						params[decodeURIComponent(param[0])] = typeof param[1] !== 'undefined' && param[1] !== '' ? decodeURIComponent(param[1]) : null;
					}
				}
			}
		}
		return params;
	};

	/**
	 * Escape HTML
	 * @param {string} string
	 * @returns {string}
	 */
	aGRID.escapeHtml = function(string) {
		if (typeof string === 'string') {
			var root = this;

			return String(string).replace(root.options.entityEscapeRegExp, function(s) {
				return root.getValueByKey(s, root.options.entityMap);
			});

		}
	};

	/**
	 * Unescape HTML
	 * @param {string} string
	 * @returns {string}
	 */
	aGRID.unescapeHtml = function(string) {
		if (typeof string === 'string') {
			var root = this;

			return String(string).replace(root.options.entityUnescapeRegExp, function(s) {
				return root.getKeyByValue(s, root.options.entityMap);
			});

		}
	};

	/**
	 * Scroll to the top of the page
	 * @param {number} [duration=1]
	 */
	aGRID.scrollTop = function(duration) {
		var step = Math.round(getPageOffset() / (duration || 1) * 10);

		/**
		 * Get current page offset
		 * @return {number}
		 */
		function getPageOffset() {
			return window.pageYOffset || (document.documentElement || document.body.parentNode || document.body).scrollTop;
		}

		/**
		 * Execute scrolling
		 */
		function scroll() {
			var pageOffset = getPageOffset();
			window.scrollTo(0, pageOffset - step);
			if (pageOffset !== 0) {

				setTimeout(function() {
					scroll();
				}, 10);

			}
		}

		scroll();
	};

	/**
	 * Start console logging
	 */
	aGRID.development = function() {
		this.options.console = true;
	};

	/**
	 * Stop console logging
	 */
	aGRID.production = function() {
		this.options.console = false;
	};

	/**
	 * Log to the array and console
	 * @returns {object}
	 */
	aGRID.log = function() {
		var argumentsLength = arguments.length;
		if (arguments[0] !== this.options.log || argumentsLength !== 1) {
			for (var i = 0; i < argumentsLength; i++) {
				this.options.log.push(arguments[i]);
			}
		}
		if (this.options.console && window.console) {
			return console.log.apply(console, arguments);
		}
	};

})();
