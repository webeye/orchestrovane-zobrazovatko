(function($) {
	'use strict';

	/**
	 * Get resources
	 */
	window.aoz.getResources = function() {
		var root = this;
		clearInterval(root.options.callback);
		root.dom.$resource.find(root.options.col).remove();
		//@todo
		$('.js-playlist-item').remove();
		$.ajax({
			type: 'GET',
			url: root.options.url + 'streamdefinition/',
			contentType: root.options.contentType,
			success: function(data) {
				var $item = {},
					dataObjectsLength = data.objects.length,
					dataObject = {};
				for (var i = 0; i < dataObjectsLength; i++) {
					//@todo
					dataObject = data.objects[i];
					$item = root.itemFactory.call(root, dataObject.resource_uri, dataObject.user_stream_id + '<br /> (' + dataObject.type + ')');
					$item.find(root.options.itemContent).append('<img class="item-content-image" src="' + dataObject.preview_url + '" alt="" />');
					root.dom.$resourceStart.after($item);
					root.dom.$playlistId.after('<span class="js-playlist-item" data-id="' + dataObject.id + '"><br /><input id="js-playlist-item-' + i + '" type="checkbox" data-location="' + dataObject.resource_uri + '" /><label for="js-playlist-item-' + i + '">' + dataObject.user_stream_id + ' (' + dataObject.type + ')</label></span>');
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(errorThrown);
			}
		});

	};

	/**
	 * Get resource
	 * @param {string} location
	 */
	window.aoz.getResource = function(location) {
		if (typeof location === 'string') {
			var root = this;
			$.ajax({
				type: 'GET',
				url: location,
				contentType: root.options.contentType,
				success: function(data) {
					root.dom.$resourceId.val(data.user_stream_id);
					root.dom.$resourceType.filter('[value="' + data.type + '"]').prop('checked', true);
					root.dom.$resourceDefinition.val(data.definition);
					root.dom.$resourceForm.show().data('type', 'put').data('location', location);
					root.dom.$overlay.show();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		} else {
			throw new Error('Invalid method arguments');
		}
	};

	/**
	 * Add resource
	 * @param {Object} resource
	 */
	window.aoz.postResource = function(resource) {
		if (typeof resource === 'object') {
			var root = this;
			$.ajax({
				type: 'POST',
				url: root.options.url + 'streamdefinition/',
				data: JSON.stringify(resource),
				contentType: root.options.contentType,
				success: function() {
					root.dom.$refresh.trigger('refresh');
					root.dom.$dialogClose.trigger('close');
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		} else {
			throw new Error('Invalid method arguments');
		}
	};

	/**
	 * Edit resource
	 * @param {Object} resource
	 * @param {string} location
	 */
	window.aoz.putResource = function(resource, location) {
		if (typeof resource === 'object' && typeof location === 'string') {
			var root = this;
			$.ajax({
				type: 'PUT',
				url: location,
				data: JSON.stringify(resource),
				contentType: root.options.contentType,
				success: function() {
					root.dom.$refresh.trigger('refresh');
					root.dom.$dialogClose.trigger('close');
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		} else {
			throw new Error('Invalid method arguments');
		}
	};

	/**
	 * Remove resource
	 * @param {string} location
	 */
	window.aoz.deleteResource = function(location) {
		if (typeof location === 'string') {
			var root = this;
			$.ajax({
				type: 'DELETE',
				url: location,
				contentType: root.options.contentType,
				success: function() {
					root.dom.$refresh.trigger('refresh');
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		} else {
			throw new Error('Invalid method arguments');
		}
	};

})(jQuery);
