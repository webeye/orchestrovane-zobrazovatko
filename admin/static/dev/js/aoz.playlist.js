(function($) {
	'use strict';

	/**
	 * Get playlists
	 */
	window.aoz.getPlaylists = function() {
		var root = this;
		clearInterval(root.options.callback);
		root.dom.$playlist.find(root.options.col).remove();
		$.ajax({
			type: 'GET',
			url: root.options.url + 'playlist/',
			contentType: root.options.contentType,
			success: function(data) {
				var dataObjectsLength = data.objects.length,
					dataObject = {};
				for (var i = 0; i < dataObjectsLength; i++) {
					dataObject = data.objects[i];
					root.dom.$playlistStart.after(root.itemFactory.call(root, dataObject.resource_uri, dataObject.playlist_name + '<br /> (playlist)'));
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(errorThrown);
			}
		});

	};

	/**
	 * Get playlist
	 * @param {string} location
	 */
	window.aoz.getPlaylist = function(location) {
		if (typeof location === 'string') {
			var root = this;
			$.ajax({
				type: 'GET',
				url: location,
				contentType: root.options.contentType,
				success: function(data) {
					root.dom.$playlistId.val(data.playlist_name);
					root.dom.$playlistForm.show().data('type', 'put').data('location', location);
					root.dom.$overlay.show();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		} else {
			throw new Error('Invalid method arguments');
		}
	};

	/**
	 * Add playlist
	 * @param {Object} playlist
	 */
	window.aoz.postPlaylist = function(playlist) {
		if (typeof playlist === 'object') {
			var root = this;
			$.ajax({
				type: 'POST',
				url: root.options.url + 'playlist/',
				data: JSON.stringify(playlist),
				contentType: root.options.contentType,
				success: function(data, textStatus, jqXHR) {
					$('.js-playlist-item').each(function() {
						var $this = $(this),
							playlistItem = {};
						if ($this.find('input:checked').length) {
							playlistItem = {
								time_frame: 10000,
								playlist: +jqXHR.getResponseHeader('Location').match(/\/([0-9]+)\/$/)[1],
								resource: +$this.data('id')
							};
							root.postPlaylistItem.call(root, playlistItem);
						}
					});
					root.dom.$refresh.trigger('refresh');
					root.dom.$dialogClose.trigger('close');
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		} else {
			throw new Error('Invalid method arguments');
		}
	};

	/**
	 * Edit playlist
	 * @param {Object} playlist
	 * @param {string} location
	 */
	window.aoz.putPlaylist = function(playlist, location) {
		if (typeof playlist === 'object' && typeof location === 'string') {
			var root = this;
			$.ajax({
				type: 'PUT',
				url: location,
				data: JSON.stringify(playlist),
				contentType: root.options.contentType,
				success: function() {
					root.dom.$refresh.trigger('refresh');
					root.dom.$dialogClose.trigger('close');
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		} else {
			throw new Error('Invalid method arguments');
		}
	};

	/**
	 * Remove playlist
	 * @param {string} location
	 */
	window.aoz.deletePlaylist= function(location) {
		if (typeof location === 'string') {
			var root = this;
			$.ajax({
				type: 'DELETE',
				url: location,
				contentType: root.options.contentType,
				success: function() {
					root.dom.$refresh.trigger('refresh');
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		} else {
			throw new Error('Invalid method arguments');
		}
	};

})(jQuery);
