/*
====================================================================================================
aGRID - Gruntfile (2015/17/04)
====================================================================================================
*/

module.exports = function(grunt) {
	var cwd = process.cwd();
	process.chdir(__dirname + '/../../..');
	grunt.loadTasks('task');
	process.chdir(cwd);
	grunt.registerTask('css', ['clean:css', 'less:css', 'autoprefixer:css', 'lesslint:css']);
	grunt.registerTask('js', ['clean:js', 'concat:js', 'uglify:js', 'jshint:js']);
	grunt.registerTask('php', ['clean:php', 'copy:php', 'htmlhint:php']);
	grunt.registerTask('lib', ['clean:lib', 'copy:lib']);
	grunt.registerTask('ajax', ['clean:ajax', 'copy:ajax']);
	grunt.registerTask('img', ['clean:img', 'copy:img']);
	grunt.registerTask('all', ['css', 'js', 'php', 'lib', 'ajax', 'img']);
};
