from django.conf.urls import patterns, include, url
from tastypie.api import Api
from admin.ui.api.resources import StreamDefinitionResource, PlaylistItemResource, PlaylistResource, ScreenResource, ScreenGroupResource
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import  settings
admin.autodiscover()


from admin.ui.views import IndexView

v1_api = Api(api_name='v1')
v1_api.register(StreamDefinitionResource())
v1_api.register(PlaylistResource())
v1_api.register(PlaylistItemResource())
v1_api.register(ScreenResource())
v1_api.register(ScreenGroupResource())

urlpatterns = patterns('',
    url(r'^index/', IndexView.as_view()),
    (r'^api/', include(v1_api.urls)),
    url(r'^admin/', include(admin.site.urls)),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
