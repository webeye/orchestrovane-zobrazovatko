from django.db import models
from django.conf import settings
import json
import requests

RESOURCE_TYPES = (
    ('HTML', 'Html'),
    ('URL', 'Url')
)

RESOURCE_STATUS = (
    ('RUNNING', 'Running'),
    ('STOPPED', 'Stopped'),
    ('STARTING', 'Starting'),
    ('STOPPING', 'Stopping')
)

PLAYLIST_STATUS = (
    ("USED", "in use"),
)


def get_ip():
    recs = StreamIp.objects.filter().all()
    if len(recs) == 0:
        recs = StreamIp(ip_counter = 0)
        recs.save()
    else:
        recs = recs[0]

    c = recs.ip_counter
    if c==(255*256*256):
        recs.ip_counter = 0
    else:
        recs.ip_counter += 1
    recs.save()
    res=[]
    res.append(c % 256)
    c = c // 256
    res.append(c % 256)
    c = c // 256
    res.append(c % 256)
    c = c // 256
    return "239.%s" % ".".join([str(i) for i in res])


class StreamIp(models.Model):
    ip_counter = models.IntegerField()


class StreamDefinition(models.Model):
    user_stream_id = models.CharField("Human readable stream resource name", max_length=255, unique=True)
    type = models.CharField("how the stream is created", max_length=10, choices=RESOURCE_TYPES)
    definition = models.TextField()
    preview_url = models.URLField()
    status = models.CharField("stream state", max_length=255)
    stream_url = models.URLField("Stream URL")

    def __str__(self):
        return str(self.user_stream_id)

    def __unicode__(self):
        return unicode(self.user_stream_id)


class ScreenGroup(models.Model):
    group_name = models.CharField("screens groups", max_length=255)

    def __str__(self):
        return str(self.group_name)


class Playlist(models.Model):
    playlist_name = models.CharField("human playlist name", max_length=255)
    status = models.CharField("Is playlist running?", max_length=255, choices=PLAYLIST_STATUS)


class PlaylistItem(models.Model):
    playlist = models.ForeignKey(Playlist)
    resource = models.ForeignKey(StreamDefinition)
    time_frame = models.IntegerField()


class Screen(models.Model):
    group = models.ForeignKey(ScreenGroup)
    screen_name = models.CharField("Screen user id", max_length=255)
    playlist = models.ForeignKey(Playlist, null=True)
    keepalive = models.TimeField("Last time screen send keep alive message")
    ip = models.IPAddressField("Screen IP address")

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(Screen, self).save(force_insert=False, force_update=False, using=None, update_fields=None)
        for item in PlaylistItem.objects.filter(playlist = self.playlist):
            resource = item.resource
            api_url = "%s/screens" % settings.STREAM_API
            stream_url = settings.STREAM_URL_TEMPLATE % get_ip()
            data = json.dumps({
                "streaming_addr" : stream_url,
                "id": resource.id,
                "url": resource.definition,
                "width":settings.STREAM_WIDTH,
                "height":settings.STREAM_HEIGHT,
                "fps":settings.STREAM_FPS,
                "key_frame":settings.STREAM_G,
            })

            response = requests.post(api_url, data)
            resp_d = json.loads(response.content)
            resource.preview_url = "%s%s" % (settings.STREAM_API, resp_d['screenshot'])
            resource.stream_url = resp_d['stream']
            print stream_url
            resource.save()
            server_api = "http://%s:%s/api/v1" % (self.ip, str(settings.SERVER_PORT))
            api_rpi = "%s/" % "/".join([server_api, "screen", "stream", "start"])
            data_rpi = json.dumps({
                'stream': resource.stream_url
            })
            requests.post(api_rpi, data_rpi)
