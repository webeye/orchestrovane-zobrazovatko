__author__ = 'rak'


from tastypie.resources import ModelResource, Serializer
from tastypie.authorization import Authorization
from admin.ui.models import Playlist, PlaylistItem, StreamDefinition, Screen, ScreenGroup
from django.conf.urls import patterns, include, url
from django.conf import settings
from tastypie.utils import trailing_slash
import json
import subprocess
from datetime import  datetime

class PlaylistResource(ModelResource):
    class Meta:
        queryset = Playlist.objects.all()
        allowed_methods = ['get', 'post', 'put', 'delete']
        serializer = Serializer(formats=['json'])
        authorization = Authorization()


class PlaylistItemResource(ModelResource):
    class Meta:
        queryset = PlaylistItem.objects.all()
        allowed_methods = ['get', 'post', 'put', 'delete']
        authorization = Authorization()


class StreamDefinitionResource(ModelResource):
    class Meta:
        queryset = StreamDefinition.objects.all()
        allowed_methods = ['get', 'post', 'put', 'delete']
        authorization = Authorization()


class ScreenResource(ModelResource):
    class Meta:
        queryset = Screen.objects.all()
        allowed_methods = ['get', 'post', 'put', 'delete']
        authorization = Authorization()

    def prepend_urls(self):
        """ Add the following array of urls to the GameResource base urls """
        return [
            url(r"^(?P<resource_name>%s)/register%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('register'), name="api_screen_register"),
            url(r"^(?P<resource_name>%s)/register%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('register'), name="api_screen_register"),
            url(r"^(?P<resource_name>%s)/stream/start%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('stream_start'), name="api_start_stream"),
            # url(r"^/stream/stop%s$" %
            #     (self._meta.resource_name, trailing_slash()),
            #     self.wrap_view('stream_stop'), name="api_stop_stream"),
        ]

    def stream_start(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        data = json.loads(request.body)
        cmd = [settings.RPI_COMMAND, data['stream']]
        print cmd
        subprocess.Popen(cmd)
        return self.create_response(request, [])

    # def stream_stop(self, request, **kwargs):
    #     self.method_check(request, allowed=['post'])
    #     data = json.loads(request.body)
    #     subprocess.Popen([settings.RPI_COMMAND, 'stop', data['stream']])

    def register(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        screen_data = json.loads(request.body)
        scr_name = screen_data.get("name", None)
        scr_group = screen_data.get("group", "free")
        scr_ip = screen_data.get("ip", "127.0.0.1")
        try:
            s_group = ScreenGroup.objects.get(group_name__exact=scr_group)
        except ScreenGroup.DoesNotExist:
            s_group = ScreenGroup(group_name=scr_group)
            s_group.save()

        try:
            screen = Screen.objects.get(screen_name__exact=scr_name)
            screen.keepalive = datetime.now()
            screen.ip = scr_ip
            screen.save()
        except Screen.DoesNotExist:
            screen = Screen(screen_name=scr_name, group=s_group, keepalive=datetime.now(), ip=scr_ip)
            screen.save()

        return self.create_response(request, {'id': screen.id})


class ScreenGroupResource(ModelResource):
    class Meta:
        queryset = ScreenGroup.objects.all()
        allowed_methods = ['get', 'post', 'put', 'delete']
        authorization = Authorization()
