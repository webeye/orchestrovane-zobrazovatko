# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'StreamDefinition'
        db.create_table(u'ui_streamdefinition', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user_stream_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('definition', self.gf('django.db.models.fields.TextField')()),
            ('preview_url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'ui', ['StreamDefinition'])

        # Adding model 'ScreenGroup'
        db.create_table(u'ui_screengroup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('group_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'ui', ['ScreenGroup'])

        # Adding model 'Playlist'
        db.create_table(u'ui_playlist', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('playlist_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'ui', ['Playlist'])

        # Adding model 'PlaylistItem'
        db.create_table(u'ui_playlistitem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('playlist', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ui.Playlist'])),
            ('resource', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ui.StreamDefinition'])),
            ('time_frame', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'ui', ['PlaylistItem'])

        # Adding model 'Screen'
        db.create_table(u'ui_screen', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ui.ScreenGroup'])),
            ('screen_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('playlist', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ui.Playlist'], null=True)),
            ('keepalive', self.gf('django.db.models.fields.TimeField')()),
        ))
        db.send_create_signal(u'ui', ['Screen'])


    def backwards(self, orm):
        # Deleting model 'StreamDefinition'
        db.delete_table(u'ui_streamdefinition')

        # Deleting model 'ScreenGroup'
        db.delete_table(u'ui_screengroup')

        # Deleting model 'Playlist'
        db.delete_table(u'ui_playlist')

        # Deleting model 'PlaylistItem'
        db.delete_table(u'ui_playlistitem')

        # Deleting model 'Screen'
        db.delete_table(u'ui_screen')


    models = {
        u'ui.playlist': {
            'Meta': {'object_name': 'Playlist'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'playlist_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'ui.playlistitem': {
            'Meta': {'object_name': 'PlaylistItem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'playlist': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ui.Playlist']"}),
            'resource': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ui.StreamDefinition']"}),
            'time_frame': ('django.db.models.fields.IntegerField', [], {})
        },
        u'ui.screen': {
            'Meta': {'object_name': 'Screen'},
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ui.ScreenGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keepalive': ('django.db.models.fields.TimeField', [], {}),
            'playlist': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ui.Playlist']", 'null': 'True'}),
            'screen_name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'ui.screengroup': {
            'Meta': {'object_name': 'ScreenGroup'},
            'group_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'ui.streamdefinition': {
            'Meta': {'object_name': 'StreamDefinition'},
            'definition': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'preview_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'user_stream_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        }
    }

    complete_apps = ['ui']