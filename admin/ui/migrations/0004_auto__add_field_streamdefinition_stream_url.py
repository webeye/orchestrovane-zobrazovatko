# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'StreamDefinition.stream_url'
        db.add_column(u'ui_streamdefinition', 'stream_url',
                      self.gf('django.db.models.fields.URLField')(default='a', max_length=200),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'StreamDefinition.stream_url'
        db.delete_column(u'ui_streamdefinition', 'stream_url')


    models = {
        u'ui.playlist': {
            'Meta': {'object_name': 'Playlist'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'playlist_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'ui.playlistitem': {
            'Meta': {'object_name': 'PlaylistItem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'playlist': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ui.Playlist']"}),
            'resource': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ui.StreamDefinition']"}),
            'time_frame': ('django.db.models.fields.IntegerField', [], {})
        },
        u'ui.screen': {
            'Meta': {'object_name': 'Screen'},
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ui.ScreenGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15'}),
            'keepalive': ('django.db.models.fields.TimeField', [], {}),
            'playlist': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ui.Playlist']", 'null': 'True'}),
            'screen_name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'ui.screengroup': {
            'Meta': {'object_name': 'ScreenGroup'},
            'group_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'ui.streamdefinition': {
            'Meta': {'object_name': 'StreamDefinition'},
            'definition': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'preview_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'stream_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'user_stream_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'ui.streamip': {
            'Meta': {'object_name': 'StreamIp'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip_counter': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['ui']