from django.contrib import admin
from models import Screen, ScreenGroup, PlaylistItem, Playlist, StreamDefinition, StreamIp
# Register your models here.

class ScreenAdmin(admin.ModelAdmin):
    list_display= ('id', 'group', 'screen_name', 'ip', 'keepalive')


class ScreenGroupAdmin(admin.ModelAdmin):
    list_display= ('id', 'group_name')

class PlaylistAdmin(admin.ModelAdmin):
    list_display = ('id', 'playlist_name', 'status')

class PlaylistItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'time_frame', 'resource')


class StreamDefinitionAdmin(admin.ModelAdmin):
    list_display = ('id', 'type', 'definition', 'preview_url', 'status', 'stream_url')

class StreamIpAdmin(admin.ModelAdmin):
    list_display = ('id', 'ip_counter')


admin.site.register(Screen, ScreenAdmin)
admin.site.register(ScreenGroup, ScreenGroupAdmin)
admin.site.register(Playlist, PlaylistAdmin)
admin.site.register(PlaylistItem, PlaylistItemAdmin)
admin.site.register(StreamDefinition, StreamDefinitionAdmin)
admin.site.register(StreamIp, StreamIpAdmin)
