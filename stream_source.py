'''
Orchestrovane Zobrazovatko Video Source

by cvrcek@avast.com

@package parkathon_zobrazovatko
'''

import xvfbwrapper, selenium.webdriver, argparse, web, json, os, shutil, subprocess

default_width = 1280
default_height = 720
default_fps = 10
default_key_frame = 40
default_top_hide = 60
default_right_hide = 15
default_streaming_addr = "udp://@239.0.0.1:5000"


screens = {}

class screen_list:
    def GET(self):
        web.header('Content-Type', 'application/json', unique=True)
        return json.dumps(list(screens.keys()))
    def POST(self):
        web.header('Content-Type', 'application/json', unique=True)

        data = json.loads(web.data())
        screen_id = str(data.get("id", 0))

        if not screen_id in screens:
            streaming_addr = data.get("streaming_addr", default_streaming_addr)
            url = data.get("url", None)
            width = data.get("width", default_width)
            height = data.get("height", default_height)
            fps = data.get("fps", default_fps)
            key_frame = data.get("key_frame", default_key_frame)
            top_hide = data.get("top_hide", default_top_hide)
            right_hide = data.get("right_hide", default_right_hide)

            screen_instance = OrchestrovaneZobrazovatkoVideoSource(instance_id = screen_id,
                                                                   streaming_addr = streaming_addr,
                                                                   width = width,
                                                                   height = height,
                                                                   fps = fps,
                                                                   key_frame = key_frame,
                                                                   top_hide = top_hide,
                                                                   right_hide = right_hide)
            screens[screen_id] = screen_instance
            screens[screen_id].start_stream(url = url)

        return json.dumps({"id": screen_id, "screenshot": "/screens/" + str(screen_id) + "/screenshot.jpg", "stream": screens[screen_id].streaming_addr})

class screen:
    def GET(self, id):
        id = str(id)
        web.header('Content-Type', 'application/json', unique=True)

        if not id in screens:
            web.notfound()
            return json.dumps({})
        else:
            return json.dumps({"id": id, "screenshot": "/screens/" + str(id) + "/screenshot.jpg", "stream": screens[id].streaming_addr})


    def POST(self, id):
        id = str(id)
        web.header('Content-Type', 'application/json', unique=True)
        data = json.loads(web.data())

        if not id in screens:
            web.notfound()
            return json.dumps({})
        else:
            if data == "stop":
                screens[id].stop_stream()
                del screens[id]
            elif data == "take_screenshot":
                screens[id].take_screenshot()

            return json.dumps(True)

class screenshot:
    def GET(self, id):
        id = str(id)
        web.header("Content-Type", "image/jpeg", unique=True)
        if not id in screens:
            web.notfound()
            return json.dumps({})
        else:
            return open(screens[id].screenshot_path,"rb").read()

class OrchestrovaneZobrazovatkoVideoSource():
    def __init__(self, instance_id, streaming_addr, width, height, fps, key_frame, top_hide, right_hide):
        self.stream_top_hide = top_hide
        self.stream_right_hide = right_hide
        self.stream_width = width
        self.stream_height = height
        self.stream_fps = fps
        self.stream_key_frame = key_frame
        self.streaming_addr = streaming_addr

        self.id = instance_id
        self.stream_folder = os.path.realpath(__file__).split(os.path.sep)
        self.stream_folder[-1] = "stream_data"
        self.stream_folder.append(str(self.id))
        self.stream_folder = os.path.sep.join(self.stream_folder)
        if not os.path.exists(self.stream_folder):
            os.makedirs(self.stream_folder)
    def __del__(self):
        shutil.rmtree(self.stream_folder)

    def start_screen(self):
        self.screen = xvfbwrapper.Xvfb(width = self.stream_width, height = self.stream_height)
        self.screen.start()

    def start_browser(self, url = None):
        self.browser = selenium.webdriver.Chrome()
        self.browser.set_window_size(width = self.stream_width + self.stream_right_hide, height = self.stream_height + self.stream_top_hide)
        self.browser.set_window_position(x = 0, y = -self.stream_top_hide)
        if url:
            self.browser.get(url)

    def stop_browser(self):
        self.browser.close()

    def stop_screen(self):
        self.screen.stop()

    def hide_cursor(self):
        self.unclutter_proc = subprocess.Popen(["unclutter",
                                                "-root",
                                                "-idle",
                                                "0",
                                                "-reset"])

    def show_cursor(self):
        self.unclutter_proc.kill()

    def start_ffmpeg(self):
        self.ff_proc = subprocess.Popen(["ffmpeg",
                                         "-f", "x11grab",
                                         "-framerate", str(self.stream_fps),
                                         "-video_size", str(self.stream_width) + "x" + str(self.stream_height),
                                         "-i", ":" + str(self.screen.vdisplay_num) + ".0+0,0",
                                         "-vcodec", "libx264",
                                         "-preset", "veryfast",
                                         "-maxrate", "1984k",
                                         "-bufsize", "3968k",
                                         "-vf", "format=yuv420p",
                                         "-g", str(self.stream_key_frame),
                                         "-f", "mpegts",
                                         str(self.streaming_addr)])

    def stop_ffmpeg(self):
        self.ff_proc.kill()

    def take_screenshot(self):
        self.screenshot_path = os.path.join(self.stream_folder, "screenshot.jpg")
        os.system("import -geometry 320x180 -quality 80 -window root " + self.screenshot_path)

    def start_stream(self, url = None):
        self.start_screen()
        if url:
            self.start_browser(url = url)
        self.hide_cursor()
        self.start_ffmpeg()
        self.take_screenshot()

    def stop_stream(self):
        try:
           self.stop_ffmpeg()
        except:
            pass
        try:
           self.show_cursor()
        except:
            pass
        try:
            if hasattr(self, "browser"):
                self.stop_browser()
        except:
            pass
        self.stop_screen()


if __name__ == "__main__":

    urls = (
        '/screens', 'screen_list',
        '/screens/([0-9]+)', 'screen',
        '/screens/([0-9]+)/screenshot.jpg', 'screenshot'
    )

    app = web.application(urls, globals())
    app.run()
